<?php
namespace DataWidget\extensible;

use DateTime;
use DataWidget\contract\FilterHandler;

/**
 * 从数组中直接过滤出数据
 */
class ArrayFilterHandler implements FilterHandler
{
    public $data;
    public function __construct($data)
    {
        $this->data=$data;
    }
    public function filter($method, $field, $condition)
    {
        if(method_exists($this,$method)){
            return call_user_func([$this, $method], $field, $condition);
        }
    }
    /** 过滤指定字段的值与条件字符串相同的数据 */
    public function equal($field,$condition)
    {
        foreach($this->data as $index => $item){
            if($item[$field] !== $condition) unset($this->data[$index]);
        }
    }
    /**
     * 过滤指定字段包含指定字符串的数据
     * @param string $field 要过滤的字段
     *      允许使用`,`分隔传入多个字段，OR关系
     *      此处的字段是指数据中的字段，而不是表单部件中的字段，一个表单字段可能控制多个数据字段
     * @param string $condition 过滤条件，字符串
     */
    public function like($field,$condition)
    {
        $fields=explode(",",$field);
        foreach($this->data as $index => $item){
            $flag=false;
            foreach($fields as $code){
                if(strpos($item[$code], $condition) !== false){
                    $flag=true;
                    break;
                }
            }
            if(!$flag) unset($this->data[$index]);
        }
    }
    /**
     * 过滤指定字段位于指定日期的数据
     * @param string $field 要过滤的字段
     * @param string $condition 过滤条件，时间戳
     */
    public function date($field,$condition)
    {
        $dt=new DateTime();
        $dt->setTimestamp($condition);
        if($dt){

            $start=$dt->setTime(0,0,0)->getTimestamp();
            $end=$dt->setTime(24,0,0)->getTimestamp();
            foreach($this->data as $index => $item){
                if($item[$field] < $start || $item[$field] > $end){
                    unset($this->data[$index]);
                }
            }
        }
    }
    /**
     * 过滤指定字段位于指定时间范围
     * @param string $field 要过滤的字段
     * @param string $condition 过滤条件，时间(Y-m-d H:i:s)
     */
    public function between($field,$condition)
    {
        if($condition["start"]) $this->after($field,$condition["start"]);
        if($condition["end"]) $this->before($field,$condition["end"]);
    }
    public function after($field,$condition)
    {
        $dt=date_create_from_format("Y-m-d H:i:s",$condition);
        if($dt){
            $condition=$dt->getTimestamp();
            foreach($this->data as $index => $item){
                if($item[$field] < $condition){
                    unset($this->data[$index]);
                }
            }
        }
    }
    public function before($field,$condition)
    {
        $dt=date_create_from_format("Y-m-d H:i:s",$condition);
        if($dt){
            $condition=$dt->getTimestamp();
            foreach($this->data as $index => $item){
                if($item[$field] > $condition){
                    unset($this->data[$index]);
                }
            }
        }
    }
    /** 字段是单选，过滤条件也是单选，返回 字段值与过滤条件相同的数据 */
    public function select($field,$condition)
    {

    }
    /** 字段是单选，过滤条件是多选，返回 字段值包含在过滤条件中的数据 */
    /** 字段是多选，过滤条件是多选，返回 字段值与过滤条件中显式指定的值完全相同的数据 */
    /** 字段是多选，过滤条件是多选，返回 字段值包含所有过滤条件中为是的值的数据*/
    // public function multiSelectFilter($field,$condition)
    // {
    //     $selected = explode(",",$condition);
    // }
}
