<?php
namespace DataWidget\field;

/**
 * 文字字段
 * 数据类型为字符串
 * 提供正则匹配功能
 */
class TextField extends Field
{
    /** @var string $regexp 正则表达式 */
    public $regexp="";
}
