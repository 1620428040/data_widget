<?php
namespace DataWidget\field;

/**
 * 字段(基类)
 * 字段本身不包含数据，只负责将传入的数据转换为指定的类型
 * 数据类型为字符串、数字等不需要特别处理的字段
 */
class Field
{
    /** @var string $code 作为提交数据时的name */
    public $code="";
    /** @var string $title 作为表单中的label，表格中的th */
    public $title="";
    /** @var string $type 前端输入控件类型 */
    public $type="";
    /** @var string $describe 提示用户的简单描述 */
    public $describe="";
    /** @var string[] $attributes 属性 */
    public $attributes=[];

    public function __construct($data=[])
    {
        foreach($data as $key => $value){
            if(in_array($key, ["code", "title", "type", "describe"])){
                $this->$key = $value;
            }
            else{
                $this->attributes[$key] = $value;
            }
        }
    }
    /** 从数组中加载 */
    static public function from($data)
    {
        return new static($data);
    }
    /** 转换为数组 */
    public function toArray()
    {
        $data=[];
        foreach($this as $key => $value){
            $data[$key]=$value;
        }
        $attributes=[];
        foreach($this->attributes as $key =>$value){
            $attributes[] = "$key='$value'";
        }
        $data["attributes"] = implode(" ",$attributes);
        return $data;
    }
    /**
     * 创建对象
     * @deprecated PHP不支持重载
     */
    static public function create($code, $title, $type="", $describe="")
    {
        $instance=new static;
        $instance->code=$code;
        $instance->title=$title;
        if($type) $instance->type=$type;
        if($describe) $instance->describe=$describe;
        return $instance;
    }
    /**
     * 构建数组
     * @deprecated PHP不支持重载
     */
    static public function build($code, $title, $type="", $describe="")
    {
        return static::create($code, $title, $type, $describe)->toArray();
    }
    /** 补全数组 */
    static public function fill($item)
    {
        return (new static($item))->toArray();
    }
    /** 补全数组列表 */
    static public function fillList($list)
    {
        $result=[];
        foreach($list as $index => $item){
            $result[$index] = (new static($item))->toArray();
        }
        return $result;
    }
    /** 从字符串数组中加载对象 */
    static public function load($data)
    {
        return new static($data);
    }
    /** 将对象归档为字符串数组 */
    public function archive()
    {
        return $this->toArray();
    }

    /** 将值字符串化 */
    public function stringify($value)
    {
        return $value;
    }
    /**
     * 解析字符串获取值
     * 字符串化的逆操作，且有些字段无法实现，默认返回null
     * 可能失败，如果失败返回null
     * @todo 是不是要放在interface中
     */
    public function parse($value)
    {
        return $value;
    }
    /** 
     * 检查字符串格式是否正确
     * @deprecated 解析时出现错误会自动返回null
     */
    public function check($value)
    {
        return true;
    }
}
