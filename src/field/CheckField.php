<?php
namespace DataWidget\field;

class CheckField extends Field
{
    /**
     * @var string[] $options
     * 字符串状态下格式为`是|否`
     */
    public $options=[];
    public function __construct($data)
    {
        parent::__construct($data);
        if(isset($data["options"])){
            $options = explode("|",$data["options"]);
            if(count($options) === 2){
                $this->options = $options;
            }
        }
        if(!$this->options) $this->options = ["是","否"];
    }
    public function stringify($value)
    {
        return $value ? $this->options[0] : $this->options[1];
    }
}
