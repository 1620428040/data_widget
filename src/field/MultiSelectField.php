<?php
namespace DataWidget\field;

use DataWidget\entity\Option;

/**
 * 选择字段
 * 数据类型为选中的选项的code
 */
class MultiSelectField extends SelectField
{
    public function stringify($value)
    {
        $selected = explode(",",$value);
        $titles=[];
        foreach($this->options as $option){
            if(in_array($option->code, $selected)) $titles[] =$option->title;
        }
        return implode(",",$titles);
    }
    /** @return null|string */
    public function parse($value)
    {
        return null;
    }
}
