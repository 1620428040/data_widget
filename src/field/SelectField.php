<?php
namespace DataWidget\field;

use DataWidget\entity\Option;

/**
 * 选择字段
 * 数据类型为选中的选项的code
 */
class SelectField extends Field
{
    /** @var Option[] $options 选项 */
    public $options=[];
    public function __construct($data)
    {
        parent::__construct($data);
        $this->options=Option::fromString($data["options"]);
    }
    public function toArray()
    {
        $data=parent::toArray();
        $options=[];
        foreach($this->options as $option){
            $options[]=$option->toArray();
        }
        $data["options"]=$options;
        return $data;
    }
    public function stringify($value)
    {
        foreach($this->options as $option){
            if(strval($value) === $option->code) return $option->title;
        }
        return "";
    }
    /** @return null|string */
    public function parse($value)
    {
        return null;
    }
}
