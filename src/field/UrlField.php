<?php
namespace DataWidget\field;

class UrlField
{
    /** 将值字符串化 */
    public function stringify($value)
    {
        return $value;
    }
    /**
     * 解析字符串获取值
     * 字符串化的逆操作，且有些字段无法实现，默认返回null
     * 可能失败，如果失败返回null
     * @todo 是不是要放在interface中
     */
    public function parse($value)
    {
        return $value;
    }
}
