<?php
namespace DataWidget\field;

use DateTime;

/**
 * 时间字段
 * 数据类型为时间戳
 */
class TimeField extends Field
{
    /** @var string $format 时间的格式 */
    public $format="Y-m-d H:i:s";
    public function __construct($data)
    {
        $this->format = $data["format"];
        unset($data["format"]);
        parent::__construct($data);
    }
    /** 将值字符串化 */
    public function stringify($value)
    {
        return date($this->format,$value);
    }
    /** @return null|int */
    public function parse($value,$format=null)
    {
        $format = $format ?: $this->format;
        if(!$dt=DateTime::createFromFormat($format,$value)) return null;
        return $dt->getTimestamp();
    }
}
