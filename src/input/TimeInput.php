<?php
namespace DataWidget\input;

use DataWidget\field\TimeField;

class TimeInput extends Input
{
    static public function getDefaultFieldClass()
    {
        return TimeField::class;
    }
    /** @var TimeField $field */
    public $field=null;
    public function __construct($data=[])
    {
        $this->field=TimeField::from($data);
    }
    /**
     * 处理相应控件返回的数据
     * 虽然只需要相应控件的输入，但是为了某些特殊情况，输入和输出的数据时所有控件的
     * @param array $input 所有控件的输入值
     * @param array $result 前面的字段已处理过的值
     * @return bool
     */
    public function input($input, &$result=[])
    {
        if(isset($input[$this->code])){
            $date=date_create_from_format($this->format,$input[$this->code]);
            if(!$date) return false;
            $this->value = $date->getTimestamp();
            $result[$this->code] = $this->value;
        }
        return true;
    }
}
