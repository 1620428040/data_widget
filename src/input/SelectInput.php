<?php
namespace DataWidget\input;

use DataWidget\field\SelectField;

class SelectInput extends Input
{
    static public function getDefaultFieldClass()
    {
        return SelectField::class;
    }
    /** @var SelectField $field */
    public $field=null;
    public function __construct($data=[])
    {
        $this->field=SelectField::from($data);
    }
    public $value="";
    public function setValue($value)
    {
        $this->value=$value;
        foreach($this->options as $option){
            $option->active = $option->code === strval($this->value);
        }
        return $this;
    }
    /**
     * 处理相应控件返回的数据
     * 虽然只需要相应控件的输入，但是为了某些特殊情况，输入和输出的数据时所有控件的
     * @param array $input 所有控件的输入值
     * @param array $result 前面的字段已处理过的值
     * @return bool
     */
    public function input($input, &$result=[])
    {
        $result[$this->code] = $this->value = isset($input[$this->code]) ? $input[$this->code] : null;
        return true;
    }
}
