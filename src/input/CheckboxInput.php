<?php
namespace DataWidget\input;

use DataWidget\field\CheckField;

class CheckboxInput extends Input
{
    static public function getDefaultFieldClass()
    {
        return CheckField::class;
    }
    public function input($input, &$result=[])
    {
        $result[$this->code] = $this->value = isset($input[$this->code]);
        return true;
    }
}
