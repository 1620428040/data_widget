<?php
namespace DataWidget\input;

use DataWidget\field\TimeField;
use DateTime;

/** 使用日期输入框和时间输入框组合成的日期时间输入框 */
class DateTimeLocalInput extends Input
{
    static public function getDefaultFieldClass()
    {
        return TimeField::class;
    }
    /** @var TimeField $field */
    public $field=null;
    public $local_format="Y-m-d";
    public function __construct($data=[])
    {
        if(isset($data["local_format"])){
            $this->local_format = $data["local_format"];
            unset($data["local_format"]);
        }
        $this->field=TimeField::from($data);
    }
    public function toArray()
    {
        $data=$this->field->toArray();
        if($this->value){
            $dt = new DateTime();
            $dt->setTimestamp($this->value);
            $data["value"] = $dt->format($this->local_format);
        }
        else{
            $data["value"]="";
        }
        return $data;
    }
    /**
     * 处理相应控件返回的数据
     * 虽然只需要相应控件的输入，但是为了某些特殊情况，输入和输出的数据时所有控件的
     * @param array $input 所有控件的输入值
     * @param array $result 前面的字段已处理过的值
     * @return bool
     */
    public function input($input, &$result=[])
    {
        if(isset($input[$this->code])){
            $dt=date_create_from_format($this->local_format, $input[$this->code]);
            if(!$dt) return false;
            $this->value = $dt->getTimestamp();
            $result[$this->code] = $this->value;
        }
        return true;
    }
}
