<?php
namespace DataWidget\input;

use DataWidget\field\Field;

/**
 * 将字段对象封装，给前端的输入控件提供数据，并处理相应控件返回的数据
 * @property string $code 作为提交数据时的name
 * @property string $title 作为表单中的label，表格中的th
 * @property string $type 前端输入控件类型
 * @property string $describe 提示用户的简单描述
 */
class Input
{
    static public function getDefaultFieldClass()
    {
        return Field::class;
    }
    /** @var Field $field */
    public $field=null;
    public function __construct($data=[])
    {
        if(isset($data["field"])){
            $field = $data["field"];
            unset($data["field"]);
            if(class_exists($field)){
                $this->field = $field::from($data);
            }
        }
        if(!$this->field){
            $field =static::getDefaultFieldClass();
            $this->field = $field::from($data);
        }
    }
    public function __get($name)
    {
        return $this->field->$name;
    }
    static public function from($data)
    {
        return new static($data);
    }
    public function toArray()
    {
        $data=$this->field->toArray();
        if($this->value){
            $data["value"]=$this->field->stringify($this->value);
        }
        else{
            $data["value"]="";
        }
        return $data;
    }

    public $value="";
    public function setValue($value)
    {
        $this->value=$value;
        return $this;
    }
    /**
     * 处理相应控件返回的数据
     * 虽然只需要相应控件的输入，但是为了某些特殊情况，输入和输出的数据时所有控件的
     * @param array $input 所有控件的输入值
     * @param array $result 前面的字段已处理过的值
     * @return bool
     */
    public function input($input, &$result=[])
    {
        $this->value = isset($input[$this->code]) ? $input[$this->code] : null;
        $result[$this->code] = $this->value;
        return true;
    }
}
