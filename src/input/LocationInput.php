<?php
namespace DataWidget\input;

use DataWidget\field\Field;
use think\console\Input;

/**
 * 地理位置输入类
 * 由于将地理位置分成多个字段，计算距离时比较方便，所以这一个控件是对应数据表中多个字段的
 */
class LocationInput extends Input
{
    /** @var Field $field 处理字段的对象 */
    public $field=null;
    public function __construct($data=[])
    {
        $this->field=Field::from($data);
    }
    
}
