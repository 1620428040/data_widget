<?php
namespace DataWidget\input;

use DataWidget\field\TimeField;
use DateTime;

/** 使用日期输入框和时间输入框组合成的日期时间输入框 */
class DateTimeCombineInput extends Input
{
    static public function getDefaultFieldClass()
    {
        return TimeField::class;
    }
    /** @var TimeField $field */
    public $field=null;
    public $date_format="Y-m-d";
    public $time_format="H:i";
    public function __construct($data=[])
    {
        if(isset($data["date_format"])){
            $this->date_format = $data["date_format"];
            unset($data["date_format"]);
        }
        if(isset($data["time_format"])){
            $this->time_format = $data["time_format"];
            unset($data["time_format"]);
        }
        $this->field=TimeField::from($data);
    }
    public function toArray()
    {
        $data=$this->field->toArray();
        if($this->value){
            $dt = new DateTime();
            $dt->setTimestamp($this->value);
            $data["value"] = [
                "date" => $dt->format($this->date_format),
                "time" => $dt->format($this->time_format),
            ];
        }
        else{
            $data["value"]=["date"=>"","time"=>""];
        }
        return $data;
    }
    /**
     * 处理相应控件返回的数据
     * 虽然只需要相应控件的输入，但是为了某些特殊情况，输入和输出的数据时所有控件的
     * @param array $input 所有控件的输入值
     * @param array $result 前面的字段已处理过的值
     * @return bool
     */
    public function input($input, &$result=[])
    {
        if(isset($input[$this->code])){
            $format = $this->date_format . " " . $this->time_format;
            $value = $input[$this->code]["date"] . " " . $input[$this->code]["time"];
            $dt=date_create_from_format($format,$value);
            if(!$dt) return false;
            $this->value = $dt->getTimestamp();
            $result[$this->code] = $this->value;
        }
        return true;
    }
}
