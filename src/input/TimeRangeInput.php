<?php
namespace DataWidget\input;

class TimeRangeInput extends Input
{
    public $start=null;
    public $end=null;
    public function input($input, &$result=[])
    {
        if(isset($input[$this->code])){
            $value=$input[$this->code];
            if(isset($value["start"])){
                $this->start=$value["start"];
            }
            if(isset($value["end"])){
                $this->end=$value["end"];
            }
        }
        $this->value = [
            "start"=>$this->start,
            "end"=>$this->end,
        ];
        $result[$this->code] = $this->value;
        return true;
    }
    public function toArray()
    {
        $arr=$this->field->toArray();
        $arr["value"]=[
            "start"=>$this->start,
            "end"=>$this->end,
        ];
        return $arr;
    }
}
