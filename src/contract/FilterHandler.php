<?php
namespace DataWidget\contract;

/**
 * 过滤器用来过滤数据的处理接口
 * 不是必须实现的接口，只要有类似的格式就可以
 * 
 * 其中的方法参数
 *  $field 
 *      此处的字段是指数据中的字段(而不是表单部件中的Input对象，一个Input可能控制多个字段，但这里只能处理一个字段)
 *      有些方法允许使用多个字段，`,`分隔，OR关系
 *  
 *      应该符合方法中指定的格式
 */
interface FilterHandler
{
    /**
     * 执行过滤操作
     * @param string $method 方法名称
     * @param string $field 要过滤的字段
     * @param string $condition 过滤条件
     */
    public function filter($method, $field, $condition);
}
