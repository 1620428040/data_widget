<?php
namespace DataWidget\contract;

interface ComparableFieldInterface
{
    /** 
     * 比较两个值
     * @param bool $is_str 值可能处于字符串状态，需要先转换
     */
    public function compare($a,$b,$is_str=false);
}
