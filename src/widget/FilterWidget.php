<?php
namespace DataWidget\widget;

use DataWidget\contract\FilterHandler;

class FilterWidget extends FormWidget
{
    public function getConditions()
    {
        $data=[];
        foreach($this->items as $item){
            $data[$item->code]=$item->getConditions();
        }
        return $data;
    }
    /** 将当前对象转换为多维数组形式，用来绑定模板 */
    public function toArray()
    {
        $items=[];
        foreach($this->inputs as $key => $item){
            $items[$key]=$item->toArray();
        }
        $data=[
            "items"=>$items,
        ];
        return $data;
    }

    /** @var FilterHandler $handler */
    public $handler=null;
    public function setHandler($handler)
    {
        $this->handler=$handler;
        return $this;
    }

    public function setInputs($data)
    {
        $this->_inputs = $data;
        return parent::setInputs($data);
    }
    public function input($data)
    {
        foreach($this->inputs as $index => $input){
            $input->input($data,$result);
            $type = $input->type ?: "text";
            $code = $input->code;
            if(isset($result[$code]) && $result[$code] !==''){
                $info=$this->map[$type];
                $this->handler->filter($info["method"], $this->_inputs[$index]["asso_field"], $result[$code]);
            }
        }
        return $this;
    }
}
