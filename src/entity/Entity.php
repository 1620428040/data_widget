<?php
namespace DataWidget\entity;

class Entity
{
    static public function create($data=[])
    {
        $instance=new self;
        if($data) $instance->fromArray($data);
        return $instance;
    }
    public function fromArray($data)
    {
        foreach($this as $key => $value){
            if(isset($data[$key])) $this->$key=$data[$key];
            // if(isset($data[$value])) $this->$key=$data[$value];
        }
        return $this;
    }
    public function toArray()
    {
        $arr=[];
        foreach($this as $key => $value){
            $arr[$key]=$value;
        }
        return $arr;
    }
}
