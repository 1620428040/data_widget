<?php
namespace DataWidget\entity;

/** 页面链接的定义 */
class PageLink
{
    /** @var string $title 标题 */
    public $title="";
    /** @var string $href 链接 */
    public $href="";
    /** @var string $target 打开页面的位置 */
    public $target="";
    /** @var string $active 是否处于活动状态 */
    public $active=false;
    
    /**
     * 创建对象
     * @return self
     */
    static public function create($title, $href, $target='', $active=false)
    {
        $instance=new self;
        $instance->title=$title;
        $instance->href=$href;
        $instance->target=$target;
        $instance->active=$active;
        return $instance;
    }
    /**
     * 构建数组
     * @return array
     */
    static public function build($title, $href, $target='', $active=false)
    {
        return self::create($title, $href, $target, $active)->toArray();
    }
    /**
     * 用默认值填充数组
     * @param array $item
     * @return array
     */
    static public function fill($item=[])
    {
        $obj=new self;
        foreach($obj as $key => $value){
            if(isset($item[$key])){
                $obj->$key=$item[$key];
            }
        }
        return $obj->toArray();
    }
    /**
     * 使用默认值填充数组列表
     * @param array[] $list
     * @return array[]
     */
    static public function fillList($list=[])
    {
        $result=[];
        foreach($list as $item){
            $obj=new self;
            foreach($obj as $key => $value){
                if(isset($item[$key])){
                    $obj->$key=$item[$key];
                }
            }
            $result[]=$obj->toArray();
        }
        return $result;
    }
    /**
     * 转换为数组
     * @return array
     */
    public function toArray()
    {
        $arr=[];
        foreach($this as $key => $value){
            $arr[$key]=$value;
        }
        return $arr;
    }
}
