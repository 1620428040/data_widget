<?php
namespace DataWidget\entity;

class Option extends Entity
{
    /**
     * @param string $options
     * @return self[]
     */
    static public function fromString($options)
    {
        $list=explode(";",$options);
        $arr=[];
        foreach($list as $option){
            $parts=explode(",",$option);
            if(count($parts) === 2){
                $instance=new self;
                $instance->code=$parts[0];
                $instance->title=$parts[1];
                $arr[]=$instance;
            }
        }
        return $arr;
    }
    /**
     * @param self[] $options
     * @return string
     */
    static public function toString($options)
    {
        $list=[];
        foreach($options as $option){
            $list[]=$option->code . "," .$option->title;
        }
        return implode(";",$list);
    }
    public $code="";
    public $title="";
    public $active=false;
}
