<?php
namespace DataWidget\entity;

/** 操作按钮的定义 */
class ActionLink
{
    /** @var string $title 标题 */
    public $title="";
    /** 
     * @var string|callable $href 链接
     * 当传入回调函数时，则在调用时生成具体的链接，回调函数的格式由使用环境定义
    */
    public $href="";
    /** @var false|string $confirm 是否需要确认，需要确认时传入确认提示语 */
    public $confirm=false;
    /** @var string $describe 描述/提示 */
    public $describe="";
    /** @var bool $ajax */
    public $ajax=false;
    
    static public function create($title, $href, $confirm=false, $describe='', $ajax=false)
    {
        $instance=new self;
        $instance->title=$title;
        $instance->href=$href;
        $instance->confirm=$confirm;
        $instance->describe=$describe;
        $instance->ajax=$ajax;
        return $instance;
    }
    static public function build($title, $href, $confirm=false, $describe='', $ajax=false)
    {
        return self::create($title, $href, $confirm, $describe, $ajax)->toArray();
    }
    static public function fill($item=[])
    {
        $obj=new self;
        foreach($obj as $key => $value){
            if(isset($item[$key])){
                $obj->$key=$item[$key];
            }
        }
        return $obj->toArray();
    }
    static public function fillList($list=[])
    {
        $result=[];
        foreach($list as $item){
            $obj=new self;
            foreach($obj as $key => $value){
                if(isset($item[$key])){
                    $obj->$key=$item[$key];
                }
            }
            $result[]=$obj->toArray();
        }
        return $result;
    }
    public function toArray()
    {
        $arr=[];
        foreach($this as $key => $value){
            $arr[$key]=$value;
        }
        return $arr;
    }
}
