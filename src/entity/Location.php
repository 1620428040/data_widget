<?php
namespace DataWidget\entity;

class Location extends Entity
{
    /** @var float $text 位置的文字描述 */
    public $text="";
    /** @var float $longitude 经度 */
    public $longitude=0.0;
    /** @var float $latitude 纬度 */
    public $latitude=0.0;
}
