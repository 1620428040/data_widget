<?php
namespace DataWidget\list_item;

use DataWidget\field\Field;

class ListItem
{
    static public function getDefaultFieldClass()
    {
        return Field::class;
    }
    /** @var Field $field 字段 */
    protected $field=null;
    public function __get($name)
    {
        return $this->field->$name;
    }
    /** @param string $class 所用字段的类名 */
    static public function fromArray($data,$class=null)
    {
        $instance = new static();
        $class = $class ?: static::getDefaultFieldClass();
        $instance->field = $class::from($data);
        return $instance;
    }
    public function stringifyValue($value)
    {
        return $this->field->stringify($value);
    }
}
